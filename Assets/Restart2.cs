using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Restart2 : MonoBehaviour
{
    public void RestartGame()
    {
        SceneManager.LoadScene("Level2");
    }
}

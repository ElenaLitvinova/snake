using System.Collections;

using UnityEngine;

public class Spawn : MonoBehaviour
{
  

    [SerializeField]
    private GameObject obj;
    float RandX;
    float RandZ;
    
    Vector3 wheretoSpawn;
    [SerializeField]
  

    private void Start()
    {
      
        for (int i = 0; i <20; i++)
        {
            RandX = Random.Range(-50f, 60f);
            RandZ = Random.Range(-4.5f, 4.0f);
            wheretoSpawn = new Vector3(RandX, transform.position.y, RandZ);

            Instantiate(obj, wheretoSpawn, Quaternion.identity);
        }
        
    }
  




}


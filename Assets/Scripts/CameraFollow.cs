using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform Player;
    private Vector3 deltaPos;//������� ���������� �� x y z ����� ����� ���������
    void Start()
    {
        deltaPos = transform.position - Player.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Player.position + deltaPos;
    }
}

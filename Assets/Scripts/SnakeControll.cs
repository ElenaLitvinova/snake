using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class SnakeControll : MonoBehaviour
{

    public Vector3 direction;
    public float speed = 5f;

    public int score;
    public TextMeshProUGUI scoreUI;
    public GameObject Snake;

    [SerializeField] private List<Transform> _tail;
    [SerializeField] private float _distance;
    [SerializeField] private GameObject _foodPrefab;

    private object ball;
    private object position;

    [SerializeField] private GameObject _cubeeasy;
    [SerializeField] private GameObject _cubemiddle;
    [SerializeField] private GameObject _cubehard;

    //public GameObject gameOver;
    public GameObject GameOverPanel;

    void Update()
    {
        if (transform.position.z < -5.6f || transform.position.z > 5.6f)

            speed = -speed;
        else speed = 5;

        
            transform.Translate(direction * Time.deltaTime);

            float movement = Input.GetAxis("Horizontal");

            transform.position += new Vector3(0, 0, movement * speed * Time.deltaTime);


            scoreUI.text = score.ToString();
            MoveTail();
        
        
    }


        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "Ball")
            {
                Destroy(other.gameObject);
                score = score + 5;
                GameObject bone = Instantiate(_foodPrefab);
                _tail.Add(bone.transform);
            }
            else if(other.gameObject.tag == "Cubeeasy")
            {
                Destroy(other.gameObject);
              score = score - 4;
                   
               
            }

            else if (other.gameObject.tag == "Cubemiddle")
            {
                Destroy(other.gameObject);
                score = score - 9;
            }


            else if (other.gameObject.tag == "Cubehard")
             {
                 Destroy(other.gameObject);
                score = score - 16;
             }
  
          if (score <= 0)
          {
            Snake.SetActive(false);
            
            GameOverPanel.SetActive(true);
           
            
          }
            
    }

        private void MoveTail()
        {
            float sqrDistance = Mathf.Sqrt(_distance);
            Vector3 previousPosition = transform.position;

            foreach (var ball in _tail)
            {
                if ((ball.position - previousPosition).sqrMagnitude > sqrDistance)

                {
                    Vector3 currentBallPosition = ball.position;
                    ball.position = previousPosition;
                    previousPosition = currentBallPosition;
                }
                else
                {
                    break;
                }

            }


        }

         
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class LevelManage3 : MonoBehaviour
{
   public static bool levelwin;

    public GameObject LevelwinPanel;
    public GameObject SnakeHead;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "finish")
        {
            LevelwinPanel.SetActive(true);
            levelwin = true;
            SnakeHead.SetActive(false);
        }
    }
}